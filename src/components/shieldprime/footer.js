import React from 'react';


const Footer = () => {
    return (
        <>
            <div className="footer">
                <div className="">
                    <div className="d-flex">Copyright 2020 @bwp Nigeria</div>
                    <div className="d-flex" style={{ fontSize: '12px' }}>Central Office Park Novare Mall, Zone 5 Wuse Abuja</div>
                </div>
            </div>
        </>
    );
}
export default Footer;