import React from 'react';
import News from './common/news/news';
import Browick from './common/browick';
import "./component_styles/general.style.css";



const BrowickNewsContainer = () => {
    return (
        <>
            <div className="cat" style={{marginBottom:'80px'}}>

                <div className="row">
                    <div className="col-lg-8 col-12">
                        <News />
                    </div>

                    <div className="col-lg-4 col-12">
                        <Browick />
                    </div>
                </div>
            </div>

        </>
    );

}
export default BrowickNewsContainer
    ;