import React from 'react';
import "./common_styles/common_styles.css";

export default function Card({ title, img }) {
    return (
        <React.Fragment>
            <div className="container card-container">
                <div className='title'>{title}</div>

                {/* Main Card */}
                <div className="card" style={{ background: `url(${img})` }}>

                    <div className="overlay"></div>
                </div>
            </div>
        </React.Fragment>
    );
}