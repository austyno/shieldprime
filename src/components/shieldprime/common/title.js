import React from 'react';
import './common_styles/common_styles.css';


const Title = ({ text}) => {
    return (
        <>
            <div className="title">
                {text}
            </div>  
        </>
    );
 }
export default Title;