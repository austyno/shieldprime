import React from 'react';
import Timer from 'react-compound-timer';



const TimerComponent = ({ dueDate }) => {


    return (
        <>
            <Timer
                initialTime={dueDate}
                startImmediately={true}
                direction="backward"
                checkpoints={[
                    {
                        time: 0,
                        callback: () => "its happening right now",
                    },
                ]}
            >
                <React.Fragment>
                    <div className="d-flex justify-content-center">
                        <div className="">
                            <div ><Timer.Days /> days : <Timer.Hours /> hours</div>
                            {/* <div ></div> */}
                            <div className="timer"><Timer.Minutes /> Min: <Timer.Seconds /> Sec</div>
                        </div>
                    </div>
                </React.Fragment>
            </Timer>
        </>
    );
}

export default TimerComponent;

