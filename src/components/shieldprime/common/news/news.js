import React from 'react';
import Slider from 'react-animated-slider';
import '../carousel/slider-animation.css';
import '../carousel/styles.css';
import "../common_styles/common_styles.css";
import content from './newscontent';

const News = () => {
    return (
        <>
            <div className="card-container">
                <div className="card news">
                    <Slider className="slider-wrapper" autoplay>
                        {content.map((item, index) => (
                            <div
                                key={index}
                                className="slider-content"
                                style={{ background: `url('${item.image}') no-repeat center center` }}
                            >
                                <div className="inner">
                                    <h1>{item.title}</h1>
                                    <p>{item.description}</p>
                                    <button>{item.button}</button>
                                </div>
                                {/* <section>
                                        <img src={item.userProfile} alt={item.user} />
                                        <span>
                                        <strong>{item.user}</strong>
                                        </span>
                                    </section> */}
                            </div>
                        ))}
                    </Slider>

                </div>
            </div>
        </>
    );
}
export default News;