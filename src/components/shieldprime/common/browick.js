import React from 'react';
import newsImg from '../../../img/browic.png';
import Timer from './timer';
import moment from 'moment';
// brauwic


const Browick = ({when}) => {

    const dueDate = moment('2020-07-9').unix();

    // moment(str).unix();  
    return (
        <>
            <div className="card-container">

                {/* Main Card */}
                <div className="card news" style={{ background: `url(${newsImg})no-repeat` }}>

                    <div className="d-flex overlay browic">
                        <div className="">
                            <Timer dueDate={dueDate} />
                        </div>
                        <p>Remaining To</p>
                        <p>BRAUWIC commencement</p>
                    </div>
                </div>
            </div>
        </>
    );
}
export default Browick;