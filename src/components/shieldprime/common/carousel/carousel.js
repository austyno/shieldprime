import React from 'react';
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/vertical.css';
// import 'normalize.css/normalize.css';
import './slider-animation.css';
import './styles.css';



const ImgCarousel = ({ content }) => (
	
    <div>
		<Slider className="slider-wrapper" autoplay={2500}>
			{content.map((item, index) => (
				<div
					key={index}
					className="slider-content"
					style={{ background: `url('${item.image}') no-repeat center center` }}
				>
					<div className="inner">
						<h1>{item.title}</h1>
						<p>{item.description}</p>
						<button>{item.button}</button>
					</div>
					{/* <section>
						<img src={item.userProfile} alt={item.user} />
						<span>
							<strong>{item.user}</strong>
						</span>
					</section> */}
				</div>
			))}
		</Slider>
	</div>
);
export default ImgCarousel;


