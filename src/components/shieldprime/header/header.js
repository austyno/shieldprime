import React from 'react';
import Carousel from '../common/carousel/carousel';
import content from './corouselcontent';


const Header = () => {
    return (
        <>
            <Carousel content={content} duration={1000} />
        </>


    );
}
export default Header;