import React from 'react';
import Header from './header/header';
import Categories from './categories';
import BrowicNewsContainer from './browicnews';
import ConferenceTitle from './common/title';
import DropShipSports from './dropship_sports';
import Footer from './footer';



function Home() {
    return (
        <>
        <Header />
        <Categories />
        <BrowicNewsContainer />
        <DropShipSports />
        <Footer />
        </>
    );
}

export default Home;
