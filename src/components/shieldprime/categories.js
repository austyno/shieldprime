import React from 'react';
import "./component_styles/general.style.css";
import Card from './common/card';

export default function Categories() {
    return (
        <React.Fragment>
            <div className="cat">
                <div className="row">
                    <div className="col-lg-3 col-12" style={{height:'200px'}}> <Card title="Shop" img="https://s3-alpha-sig.figma.com/img/f480/7559/60b86bb996baf5e69c7c5bf7fe0322d3?Expires=1595203200&Signature=OZ~uO-MrqJ1sl-e0cu6kxviN4HcwiVVB5D0fRCjKhal9vnR1KQB3E2y96ceNMknDT~l4dFU2Ziepsxzilsjxo4-SPB26ivKzC7cRlDQC17vUVBryrjcMfZ6rB~R4cPYpw84r3lAZTX8hY7FY5hnlFGo6La9U4z~fgj1d3wIgg9GjxBY5ff-3z07Llq80IFsbJ0eniJ7FXpzmF3eX6HnAmdwKCAFBP6x5B7fPvfL4p5aeseiWYVhPlIN3gVnG9YfVC1gM5YUE1AP7P9717UR8CWIECi8jKFHnV~OLGUYXNv3EYxsXJr9ZKlh1CkOdrEGla6UkvnCBqbFlWBxTZNYBfQ__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" /> </div>
                    <div className="col-lg-3 col-12"> <Card title="Conference" /> </div>
                    <div className="col-lg-3 col-12"> <Card title="Movies" /> </div>
                    <div className="col-lg-3 col-12"> <Card title="Music" /> </div>
                </div>
            </div>
        </React.Fragment>
    );
}