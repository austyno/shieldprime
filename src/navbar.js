import React, { useEffect, useState } from 'react';
import Link from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';



const NavBar = () => {
    return (
        <Navbar sticky="top" style={{ background: 'rgba(22, 6, 86, 0.8)', fontFamily: 'Quicksand', height: '65px' }} >
            <Nav className="ml-auto pr-3">
                <Nav.Link href="#home" style={{ color: 'white',}}>Home</Nav.Link>
                <Nav.Link href="#features" style={{ color: 'white',}}>Conferences</Nav.Link>
                <Nav.Link href="#pricing" style={{ color: 'white' }}>Programs</Nav.Link>
            </Nav>
        </Navbar>
    );
}
export default NavBar;