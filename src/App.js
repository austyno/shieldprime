import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import ShieldPrimeHome from './components/shieldprime/home'
import NavBar from './navbar';



function App() {
  return (
    <Router>
      <NavBar />
      <Route exact path="/" component={ShieldPrimeHome} />
    </Router>
  );
}

export default App;
